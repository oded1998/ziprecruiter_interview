from typing import Tuple

from dependencies.logging import Log4j
from pyspark.sql import SparkSession

from dependencies import logging


def start_spark(app_name: str = 'my_spark_app', master: str = 'local[*]') -> Tuple[SparkSession, Log4j]:
    """Start Spark session, get Spark logger
    :param app_name: Name of Spark app.
    :param master: Cluster connection details (defaults to local[*]).
    :return: A tuple of references to the Spark session and logger
    """
    # get Spark session factory
    spark_builder = (
        SparkSession
            .builder
            .master(master)
            .appName(app_name))

    # create session and retrieve Spark logger object
    spark_sess = spark_builder.getOrCreate()
    spark_logger = logging.Log4j(spark_sess)

    return spark_sess, spark_logger
