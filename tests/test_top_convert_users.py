import unittest

from conf import TEST_DATA_LOCATION
from dependencies.spark import start_spark
from jobs.top_convert_users import TopConvertingUsers


class TestTopConvertUsers(unittest.TestCase):
    def setUp(self):
        self.spark, *_ = start_spark()
        self.top_converting_users = TopConvertingUsers()
        self.limit_num = 5
        
    def tearDown(self):
        """Stop Spark Session
        """
        self.spark.stop()

    def test_transform_data(self):
        # assemble
        input_data = self.spark.read.format("csv").option("header", "true").load(
            f"{TEST_DATA_LOCATION}website_events.csv")
        expected_data = self.spark.read.format("csv").option("header", "true").load(
            f"{TEST_DATA_LOCATION}top_converting_users.csv")

        expected_cols = len(expected_data.columns)

        # act
        data_transformed = self.top_converting_users.transform_data(df=input_data, limit_num=self.limit_num)

        rows = data_transformed.count()
        cols = len(data_transformed.columns)

        # assert
        self.assertEqual(expected_cols, cols)
        self.assertTrue([col in expected_data.columns
                         for col in data_transformed.columns])
        self.assertLessEqual(rows, self.limit_num)


if __name__ == '__main__':
    unittest.main()
