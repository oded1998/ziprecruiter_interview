import unittest

from conf import TEST_DATA_LOCATION
from dependencies.spark import start_spark
from jobs.fast_converting_users import FastConvertingUsers


class TestFastConvertingUsers(unittest.TestCase):
    def setUp(self):
        self.spark, *_ = start_spark()
        self.fast_converting_users = FastConvertingUsers()

    def tearDown(self):
        """Stop Spark Session
        """
        self.spark.stop()

    def test_transform_data(self):
        # assemble
        input_data = self.spark.read.format("csv").option("header", "true").load(
            f"{TEST_DATA_LOCATION}website_events.csv")
        expected_data = self.spark.read.format("csv").option("header", "true").load(
            f"{TEST_DATA_LOCATION}fast_converting_userse.csv")

        expected_cols = len(expected_data.columns)

        # act
        data_transformed = self.fast_converting_users.transform_data(df=input_data)

        cols = len(data_transformed.columns)

        # assert
        self.assertEqual(expected_cols, cols)
        self.assertTrue([col in expected_data.columns
                         for col in data_transformed.columns])


if __name__ == '__main__':
    unittest.main()
