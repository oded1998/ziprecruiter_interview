from typing import Optional
from conf import LOCAL_DATA_LOCATION, LOCAL_RESULTS_LOCATION, SPARK_LOCAL
from jobs.abc_base.job import Job
from pyspark.sql import Window, DataFrame, SparkSession
import os

from pyspark.sql.functions import col, lag, when, sum, row_number


class FastConvertingUsers(Job):
    def __init__(self, app_name: str = 'fast_converting_users', master: str = SPARK_LOCAL,
                 spark: Optional[SparkSession] = None,
                 data_path: str = LOCAL_DATA_LOCATION,
                 save_path: str = LOCAL_RESULTS_LOCATION):
        super().__init__(app_name=app_name, master=master, spark=spark)
        self.job_name = app_name
        self.data_path = data_path
        self.save_path = save_path

    def main(self) -> None:
        """
        Running the Job
        :return: None
        """
        self.log.info('Starting top conversion users job')
        data = self.extract_data()
        data_transformed = self.transform_data(data)
        self.log.info('Successfully transformed data')
        self.save_data(data_transformed)
        self.log.info('Successfully saved data')

    def extract_data(self) -> DataFrame:
        """
        Loads data to DataFrame
        :return: Input DataFrame
        """
        df = self.spark.read.format("csv").option("header", "true").load(self.data_path)

        return df

    def transform_data(self, df: DataFrame) -> DataFrame:
        """Transform original dataset.
        :param df: Input DataFrame.
        :return: Transformed DataFrame.
        """

        w = Window.partitionBy("user_id").orderBy("timestamp")

        transformed_df = df.withColumn("row_number", row_number().over(w)). \
            filter((col("type") == "start_session") | (col("type") == "conversion")). \
            withColumn("distance", col("row_number") - lag(col("row_number"), 1).over(w) - 1). \
            filter(col("type") == "conversion"). \
            sort(col("distance").desc()). \
            select(col("user_id"), col("distance"))

        return transformed_df

    def save_data(self, df: DataFrame) -> None:
        super().save_data(df)


if __name__ == '__main__':
    master = os.getenv('MASTER', SPARK_LOCAL)
    app_name = os.getenv('APP_NAME', "fast_converting_users")
    data_path = os.getenv('DATA_PATH', LOCAL_DATA_LOCATION)
    save_path = os.getenv('SAVE_PATH', LOCAL_RESULTS_LOCATION)
    fast_converting_user = FastConvertingUsers(master=master, app_name=app_name, data_path=data_path,
                                               save_path=save_path)
    fast_converting_user.main()
