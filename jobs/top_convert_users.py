import os
from typing import Optional

from conf import LOCAL_DATA_LOCATION, LOCAL_RESULTS_LOCATION, SPARK_LOCAL
from jobs.abc_base.job import Job
from pyspark.sql import DataFrame, SparkSession

from pyspark.sql.functions import col


class TopConvertingUsers(Job):
    def __init__(self, app_name: str = 'top_converting_users', master: str = SPARK_LOCAL,
                 spark: Optional[SparkSession] = None,
                 data_path: str = LOCAL_DATA_LOCATION,
                 save_path: str = LOCAL_RESULTS_LOCATION, limit_number: int = 10):
        super().__init__(app_name=app_name, master=master, spark=spark)

        self.job_name = app_name
        self.data_path = data_path
        self.save_path = save_path
        self.limit_number = limit_number

    def main(self):
        """
       Running the Job
       :return: None
       """
        self.log.info('Starting top conversion users job')
        data = self.extract_data()
        data_transformed = self.transform_data(data)
        self.save_data(data_transformed)

    def extract_data(self) -> DataFrame:
        """Load data from Parquet file format.
        :return: Spark DataFrame.
        """
        df = self.spark.read.format("csv").option("header", "true").load(self.data_path)

        return df

    def transform_data(self, df: DataFrame) -> DataFrame:
        """Transform original dataset.
        :param df: Input DataFrame.
        :return: Transformed DataFrame.
        """

        transformed_df = df.filter(col("type") == "conversion"). \
            groupBy(col("user_id")).count(). \
            select(col("user_id"), col('count').alias('conversion_count')). \
            sort(col("conversion_count").desc()) \
            .limit(self.limit_number)

        return transformed_df

    def save_data(self, df):
        super().save_data(df)


if __name__ == '__main__':
    master = os.getenv('MASTER', "local[*]")
    app_name = os.getenv('APP_NAME', "top_converting_users")
    data_path = os.getenv('DATA_PATH', LOCAL_DATA_LOCATION)
    save_path = os.getenv('SAVE_PATH', LOCAL_RESULTS_LOCATION)
    limit_number = os.getenv('LIMIT_NUMBER', 10)

    top_converting_users = TopConvertingUsers(master=master, app_name=app_name, data_path=data_path,
                                              save_path=save_path, limit_number=limit_number)
    top_converting_users.main()
