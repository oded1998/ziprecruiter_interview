from abc import ABC
from typing import Optional

from conf import LOCAL_RESULTS_LOCATION, LOCAL_DATA_LOCATION, SPARK_LOCAL
from dependencies.spark import start_spark
from pyspark.sql import DataFrame, SparkSession
from utils import get_current_datetime


class Job(ABC):
    def __init__(self, app_name='my_spark_app', master: str = SPARK_LOCAL, spark: Optional[SparkSession] = None,
                 data_path=LOCAL_DATA_LOCATION,
                 save_path=LOCAL_RESULTS_LOCATION):
        if not spark:
            self.spark, self.log = start_spark(app_name=app_name, master=master)
        else:
            self.spark = spark
        self.job_name = "default_job_name"
        self.data_path = data_path
        self.save_path = save_path

    def main(self, *args, **kwargs):
        pass

    def extract_data(self, *args, **kwargs) -> DataFrame:
        pass

    def transform_data(self, *args, **kwargs) -> DataFrame:
        pass

    def save_data(self, df: DataFrame) -> None:
        """
        Saves Dataframe in designated path
        :param df:
        :return:
        """
        df. \
            write.format("csv"). \
            option("header", "true"). \
            save(f"{self.save_path}{self.job_name}/{get_current_datetime()}")
