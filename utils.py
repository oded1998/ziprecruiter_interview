from datetime import datetime


def get_current_datetime() -> str:
    """
    Get current datetime in the formant %Y%m%d%H%M%S for example: 19120623000000
    :return: String of the current datetime
    """
    return datetime.now().strftime("%Y%m%d%H%M%S")
